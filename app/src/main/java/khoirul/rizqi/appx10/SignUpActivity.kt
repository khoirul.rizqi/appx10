package khoirul.rizqi.appx10

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signup.*

class SignUpActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        btnRegister.setOnClickListener(this)
        loginnow.setOnClickListener {
            var intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onClick(v: View?) {
        var email = edRegUserName.text.toString()
        var password = edRegPassword.text.toString()

        if(email.isEmpty() || password.isEmpty()){
            Toast.makeText(this,"Username / Password can't be empty",Toast.LENGTH_LONG).show()
        }else{
            val progressDialog = ProgressDialog(this)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage("Registering...")
            progressDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener{
                    progressDialog.hide()
                    if (!it.isSuccessful)return@addOnCompleteListener
                    Toast.makeText(this,"Succesfully Register",Toast.LENGTH_SHORT).show()
                    edRegUserName.setText("")
                    edRegPassword.setText("")
                }
                .addOnFailureListener {
                    progressDialog.hide()
                    Toast.makeText(this,"Username / Password incorrect",Toast.LENGTH_SHORT).show()
                }
        }
    }
}