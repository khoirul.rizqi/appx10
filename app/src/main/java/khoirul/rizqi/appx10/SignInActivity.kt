package khoirul.rizqi.appx10

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity : AppCompatActivity() {

    var fbAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        btnLogoff.setOnClickListener{
            fbAuth.signOut()
            var intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
            Toast.makeText(this,"You Succesfully Log Out",Toast.LENGTH_SHORT).show()
        }
    }
}